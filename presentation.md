---
title: Café multi-facettes
subtitle: Partage du projet - plateforme numérique pour tous
author: Pierre Camilleri & Tarik Amar
date: 2 mai 2024
title-slide-attributes:
    data-background-image: "static/logo.svg, static/logo_client.png"
    data-background-size: "auto 5%, auto 5%"
    data-background-position: "95% 95%, 83% 95%"
---

# Produit

À toi la parole Pierre 🎤

# Tech

## Les dessous 🔎

En gros **postgresql** & **django**

<img src="images/postgresql-logo.png" width="200" style="vertical-align: middle;" />
<span style="font-size: 3rem;">🤝</span>
<img src="images/django-logo.png" width="200" style="vertical-align: middle;" />

# Postgresql

Rien de particulier, si ce n'est la découverte de l'extension `unaccent`.

```sql
select * from model
    where unaccent('first_name') = unaccent('Pïèrre');
```

```python
User.objects.filter(first_name__unaccent="Pïèrre")
```

# python


## Gestionnaire de dépendances


<img src="images/poetry-logo.png" width="75" style="vertical-align: middle;" />
**poetry** pour la gestion des dépendances

<img src="images/poe-logo.png" width="75" style="vertical-align: middle;" />
**poe** pour créer des raccourcis de commande ~~`python manage.py…`~~

# Django

1ère utilisation de ce framework (soyez indulgent).

Un projet **django** est divisé en plusieurs *applications*, sorte de domaines.
Dans chaque application, on va retrouver les fichiers suivants :

## models.py

Décrivent les tables (colonnes, contraintes, validations…) & les formulaires (champs, contraintes validations…)

```python
…
class Structure(models.Model):
    name = models.CharField(
        "Nom",
        max_length=200,
        null=False,
        blank=False
    )
    cities = models.ManyToManyField(
        City,
        verbose_name="Communes"
    )
```

## views.py

Sorte de contrôleur, l'action menée lors de la visite.

C'est aussi ici qu'on vérifia les autorisations (via des décorateurs)

## views - Autorisation

```python
…
@permission_required(
    "beneficiaries.change_beneficiary",
    raise_exception=True
)
def edit(request, beneficiary_id):
…
```

## views.py - Initialisation de la vue et de son formulaire
```python
…
beneficiary = get_object_or_404(
    Beneficiary,
    pk=beneficiary_id
)
form = BeneficiaryForm(instance=beneficiary)

return render(
    request,
    "beneficiaries/edit.html",
    {"beneficiary": beneficiary, "form": form}
)
```

## urls.py

Le lien entre une URL et une action de contrôleur

```python=
path("nouveau", views.new, name="new"), 
```

1. Le slug
2. L'action à exécuter dans `views.py`
3. Le nom technique de cette URL, pour s'en servir ailleurs dans le code (lien, redirection…)

## app.py

De la configuration, surtout le nom de l'application (à creuser)

## admin.py

Django vient de base avec une interface *backoffice*, avec un gestionnaire par *app* / *modèle* / *table*.
Cette partie sert à personnaliser ce sous-ensemble.

<img src="images/django-admin-beneficiaries.png" style="vertical-align: middle;" />


##
On va pouvoir définir :

- les colonnes de la table à afficher (par défaut django liste la table paginée).
- dans quelles colonnes effectuer la recherche (présente aussi par défaut)
- les champs de formulaires de création et d'édition d'entité
- une action de masse, par défaut l'action supprimer est déjà en place

## django template

Rien de spécial, ce n'est pas le plus drôle

```html
{% extends "master.html" %}

{% block content %}
  <div class="card">
    <div class="card-header py-3">
      Profil de : {{ beneficiary }}
    </div>
…
```

## Extensions django

- bootstrap 5 (RAS) 🎨
- phonenumberfield ☎️
    - Vérification et reconnaissance de numéro de téléphone sous plusieurs formats

# CI

<img src="images/gitlab-logo.png" height="40" style="vertical-align: middle;" />
Gitlab lance 2 jobs :

- les tests surtout d'intégration (curl de page, POST…) & quelques rares unitaires
- un linter flake8

# Hébergement et déploiement

## fly.io

<img src="images/fly-io-logo.png" height="40" style="vertical-align: middle;" />

Découverte également, presque sans aucune friction.

Il a reconnu le projet django 5 (d'autres solution s'arrête à 4) et l'utilisation de poetry (ce qui a posé problème avec scalingo).
Il a aussi générer automatiquement un Dockerfile.

Nous avons pu configurer des instances en France (`cdg`) 🇫🇷.

## Déploiement avec fly.io

On déploie avec la commande `fly deploy` indépendamment du statut de git.

## Pour la soif

Possibilité de se connecter aux machines (l'application ou postgresql)

```sh
fly ssh console -a <name>
```

Faire un tunnel de la base de données vers sa machine locale

```sh
fly proxy 5433:5432 -a <db-app-name>
```
